# CarCar

Team:

* Karmina Barajas - Sales
* Ting Wei Wang - Services
* Perla Carlson - Inventory 



## Inventory

Our project is called Cars Cars, an application geared towards car dealerships or businesses with car inventories, as well as management services for sales and service categories. Our group initially met up as a pair and divided up the sales and service between them. I was left with the inventory portion of this app. I also contributed by helping my partner with one of her models, setting up the views and guided her through making one of her lists. 
 Coincidentally, our bounded contexts were  delineated in the 3 larger categories of divided work.  Our bounded contexts are divided by the three aggregate roots, which include: Inventory, Services, and Sales. The aggregates of Inventory include: Manufacturer, Vehicle Model, and Automobile. Now the entity in this context is Automobile, as it has the capacity to change. The value objects which are aggregates that don't change include: Manufacturer and Vehicle Model. I was tasked with creating a list for each aggregate root and forms to add additional Manufacturers, Automobiles and Vehicle Models. My approach was to start with the simplest portions which were the lists and once I got those working I was able to move on to the forms. 



## Service microservice

Explain your models and integration with the inventory
microservice, here. 

This is an automobile dealership application that manages its sales, service and inventory. There are 3 microservices/bounded contexts in this application- Sales, Inventory and Service. I am responsible for the 
Service microservice which consists of creating technician and the service appointment. Both technician and the service appointment are entities. In order for the Service to be created (needing VIN from automobile model in Inventory), we need to get the automobile information by fetching the Inventory API. We make an AutomobileVO, an value object, to pull needed VIN from the Inventory microservice via the poller file. Once backend is created, forms and list pages are created using React and we make API to fetch data from the backend to display in the front end. 


## Sales microservice

Explain your models and integration with the inventory
microservice, here.

The sales microservice handles keeping track of employee sales and inventory. The sales record form and sales list will need to fetch info from the inventory to assess whether or not a vehicle is available for sale. 





