import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              {/* <NavLink className="nav-link" aria-current="page" to="/technicians/new">Technician Form</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="/appointments">Appointment List</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="/appointments/history">Appointment History</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="/appointments/new">Appointment Form</NavLink> */}
                <NavLink className="dropdown-item" to="/technicians/new">Technician Form</NavLink>
                <div className="dropdown-divider"></div>
                <NavLink className="dropdown-item" to="/appointments/new">Appointment Form</NavLink>
            </li>
            <li className="nav-item">
            <NavLink className="dropdown-item"  to="/appointments">Appointment List</NavLink>
            <div className="dropdown-divider"></div>
            <NavLink className="dropdown-item"  to="/appointments/history">Appointment History</NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="dropdown-item" to="/sales/create">Sales Person Form</NavLink>
                <div className="dropdown-divider"></div>
                <NavLink className="dropdown-item" to="/sales/potential_customer/create">Potential Customer Form</NavLink>
                </li>
                <li className="nav-item">
                <NavLink className="dropdown-item" to="/sales/sale_record/create">Sale Record Form</NavLink>
                <div className="dropdown-divider"></div>
                <NavLink className="dropdown-item" to="/sales/sales_list">All Sales</NavLink>
                <NavLink className="dropdown-item" to="/sales/employee_sales_list">Employee Sales</NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="dropdown-item" to="/manufacturers">Manufacturer List</NavLink>
                <div className="dropdown-divider"></div>
                <NavLink className="dropdown-item" to="/manufacturers/create">Manufacturer Form</NavLink>
                </li>
                <li className="nav-item">
                <NavLink className="dropdown-item" to="/models/">Vehicle Models List</NavLink>
                <div className="dropdown-divider"></div>
                <NavLink className="dropdown-item" to="/models/create">Vehicle Models Form</NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="dropdown-item" to="/automobiles">Automobiles List</NavLink>
          <div className="dropdown-divider"></div>
                <NavLink className="dropdown-item" to="/automobiles/create">Automobile Form</NavLink>
                </li>
           
          
           
          
              
             
           
          </ul>
        

               
              
               
              
            </div>
             
              </div>
  
            
      
      </nav>
  )
}

export default Nav;
